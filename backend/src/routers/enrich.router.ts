import { Router } from "express"
import { VoiceLine } from "../models/voiceLine"
import { EnrichManager } from "../tools/enrichManager"

const router = Router()

router.post(
    "/",
    async (req, res) => {
      res.setHeader('Content-Type', 'text/plain')
      const options = {
          "type": "email",
          "length": "short"
      }
      console.log("test")
      const vl : VoiceLine = await EnrichManager.enrich("this is the trancsribed text", options)
      console.log("test2")
      res.send('This is the VoiceLine')
  }
)

export { router as enrichRouter }
