import express from 'express';
import { debugRouter } from "./routers/debug.router";
import { enrichRouter } from "./routers/enrich.router";
import { voiceLineRouter } from "./routers/voiceLine.router";

const port = 4242;
const app = express();

app.use(express.json())

app.use('/debug', debugRouter);

app.use('/enrich', enrichRouter);

app.use('/voiceline', voiceLineRouter);

app.get('/', (req, res) => {
  res.send(`Hello, World!`)
})

app.listen(port, () => {
  console.log(`Ready on port ${port}`);
});
