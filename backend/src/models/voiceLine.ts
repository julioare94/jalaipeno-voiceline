export class VoiceLine {
    transcript: string

    enrichment: string

    constructor(transcript: string, enrichment: string) {
        this.transcript = transcript
        this.enrichment = enrichment
    }
}