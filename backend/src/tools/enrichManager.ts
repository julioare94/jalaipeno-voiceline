import { VoiceLine } from "../models/voiceLine"

const completionUrl : string = "https://api.openai.com/v1/engines/{0}/completions"

export class EnrichManager {
    static async enrich(transcript: string, options: any) {
        //API requests with GPT3

        //Here comes the logic

        //Create VoiceLine object out of gotten enrichment from GPT3
        const vl : VoiceLine = new VoiceLine(transcript, "enrichment")
        return vl
    }
}