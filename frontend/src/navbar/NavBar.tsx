import { Navbar, NavbarBrand, Nav, NavItem, NavLink } from 'reactstrap'
import './NavBar.css'

import VoiceLineLogo from './../logo.svg'

const VoiceLineNavBar = (props: any) => {
  return (
    <div>
      <Navbar color="light" light expand="md">
        <NavbarBrand href="/">
          <img src={VoiceLineLogo} alt="voiceline_logo" />
        </NavbarBrand>

        <Nav className="mr-auto" navbar>
          <NavItem>
            <NavLink href="/settings">Settings</NavLink>
          </NavItem>
        </Nav>
      </Navbar>
    </div>
  )
}

export default VoiceLineNavBar
