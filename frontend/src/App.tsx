import React, { useState } from 'react'
import './App.css'
import {
  FormGroup,
  Label,
  Input,
  Card,
  CardTitle,
  Form,
  CardBody,
  Button,
} from 'reactstrap'

function App() {
  const [options, setOptions] = useState({
    language: 'english',
    style: 'short',
  })

  const [text, setText] = useState('')
  const [gptResult, setGPTResult] = useState('')

  const submitSettings = (e: any) => {
    e.preventDefault()
    console.log(text)
    console.log(options)

    // TODO: Sent options to backend
    setGPTResult('Here is the answer')
  }

  const changeLanguage = (e: any) => {
    setOptions({
      ...options,
      language: e.target.value,
    })
  }

  const changeStyle = (e: any) => {
    setOptions({
      ...options,
      style: e.target.value,
    })
  }

  return (
    <div className="settings">
      <Card>
        <CardBody>
          <CardTitle tag="h4">Settings</CardTitle>
          <Form>
            <FormGroup>
              <Label for="transcribedText">Transcribed VoiceMemo</Label>
              <Input
                type="textarea"
                name="text"
                id="transcribedText"
                onChange={(e) => setText(e.target.value)}
              />
            </FormGroup>
            <FormGroup>
              <Label for="languageSelect">Language</Label>
              <Input
                type="select"
                name="select"
                id="languageSelect"
                onChange={changeLanguage}
              >
                <option value="english">English</option>
                <option value="french">French</option>
                <option value="spanish">Spanish</option>
                <option value="german">German</option>
                <option value="hindi">Hindi</option>
                <option value="chinese">Chinese</option>
              </Input>
            </FormGroup>
            <FormGroup>
              <Label for="styleSelect">Voiceline Style</Label>
              <Input
                type="select"
                name="select"
                id="styleSelect"
                onChange={changeStyle}
              >
                <option value="short">Short Summary</option>
                <option value="bullets">Bullet Summary</option>
              </Input>
            </FormGroup>

            <Button onClick={submitSettings}>Save</Button>

            <br />
            <br />
            <FormGroup>
              <Label for="exampleText">Result</Label>
              <Input
                type="textarea"
                name="text"
                id="exampleText"
                value={gptResult}
                disabled={true}
              />
            </FormGroup>
          </Form>
        </CardBody>
      </Card>
    </div>
  )
}

export default App
